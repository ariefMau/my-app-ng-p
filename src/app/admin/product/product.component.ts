import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as FileSaver from 'file-saver';
import { ApiService } from 'src/app/services/api.service';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  title: any; //didalam function, tidak perlu this
  book: any={}; //mendefinisikan var book sebagai object
  books: any=[]; //1.membuat koleksi books

  constructor(
   public dialog: MatDialog,
   public api: ApiService
  ) 
  {
    
  }

  ngOnInit(): void 
  {
    this.title='Product';
    this.getBooks();
  }

  loading: boolean | undefined;
  getBooks()
  {
    this.loading=true;
    this.api.get('bookswithauth').subscribe(result=>{
      this.books=result;
      this.loading=false;
    }, error=>{
      this.loading=false;
      alert('tidak dapat mengambil data');
    })
  }

  productDetail(data: any, idx: number)
  {
    let dialog=this.dialog.open(ProductDetailComponent, {
      width:'400px',
      data:data
    });
    dialog.afterClosed().subscribe(res=>{
      if(res)
      {
        //jika idx=-1 (penambahan data baru) maka tambahkan data
        if(idx==-1)this.books.push(res);      
        //jika tidak maka perbarui data  
        else this.books[idx]=data; 
      }
    })
  }

  loadingDelete: any={};
  deleteProduct(idx: any, id: string)
  {
    var conf = confirm('Delete Item?');
    if (conf)
    {
      this.api.delete('books/'+id).subscribe(res=>{
        this.books.splice(idx, 1);
        this.loadingDelete[idx]=false;
        alert('data berhasil dihapus')
      }, error=>{
        alert('tidak dapat menghapus data');
        this.loadingDelete[idx]=false;
      })
    }
  }

  Upload(data: any, idx: any)
  {
    let dialog = this.dialog.open(FileUploaderComponent, {
      width: '400px',
      data: data
    });
    dialog.afterClosed().subscribe(res=>{
      return;
    })
  }

  downloadFile(data: any)
  {
    FileSaver.saveAs('http://api.sunhouse.co.id/bookstore/'+data.url);
  
  }
}
